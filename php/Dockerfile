FROM php:7.2-apache

## pour que apt soit noninteractive
ENV DEBIAN_FRONTEND noninteractive
ENV DEBCONF_NONINTERACTIVE_SEEN true

# librairies nécessaires
RUN apt-get update \
    && apt-get install -yq --no-install-recommends apt-utils wget locales git g++ systemd nano zip unzip gnupg \
    libicu-dev libpng-dev libxml2-dev libzip-dev libonig-dev libxslt-dev \
    libmemcached-dev libz-dev libldb-dev libsnmp-dev libpq-dev libjpeg-dev libfreetype6-dev libbz2-dev

# tzdata config (gestion du fuseau horaire)
RUN echo "tzdata tzdata/Areas select Europe" > /tmp/preseed.txt; \
    echo "tzdata tzdata/Zones/Europe select Paris" >> /tmp/preseed.txt; \
    debconf-set-selections /tmp/preseed.txt && \
    apt-get update && \
    apt-get install -yq tzdata

RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && \
    echo "fr_FR.UTF-8 UTF-8" >> /etc/locale.gen && \
    locale-gen

# php extensions
RUN docker-php-ext-configure intl
RUN docker-php-ext-install -j$(nproc) intl
RUN docker-php-ext-configure gd
RUN docker-php-ext-install -j$(nproc) gd
RUN docker-php-ext-install fileinfo mbstring mysqli pdo pdo_mysql xsl opcache zip xml calendar gd intl dom  bcmath bz2
RUN docker-php-ext-enable intl gd opcache
RUN pecl install apcu && docker-php-ext-enable apcu
RUN yes | pecl install xdebug \
    && echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)" > /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.remote_enable=on" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.remote_autostart=off" >> /usr/local/etc/php/conf.d/xdebug.ini

# apache2 config
RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf
RUN apt-get install -yq libapache2-mod-fcgid
RUN a2enmod proxy_fcgi setenvif
RUN a2enmod proxy proxy_http
RUN a2enmod rewrite
RUN a2enmod authz_core
RUN a2enmod expires
RUN a2enmod ssl
RUN a2enmod headers
RUN a2enmod deflate

# apache2 environment variables
ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2
ENV APACHE_LOCK_DIR /var/lock/apache2
ENV APACHE_PID_FILE /var/run/apache2.pid

# firewall (apache2)
RUN apt install -yq ufw
RUN ufw allow 80/tcp comment 'accept Apache'
RUN ufw allow 443/tcp comment 'accept HTTPS connections'

# On copie la configruation php.ini du projet
COPY php.ini /usr/local/etc/php/php.ini

# composer
ENV COMPOSER_ALLOW_SUPERUSER=1
RUN curl -sS https://getcomposer.org/installer | php -- && \
   mv composer.phar /usr/local/bin/composer
RUN composer self-update --1

# node 16 (+npm)
RUN curl -sL https://deb.nodesource.com/setup_16.x | bash -
RUN apt-get install -yq nodejs

# Envrionnement developpement
ENV PATH $PATH:/root/

WORKDIR /var/www/html/
