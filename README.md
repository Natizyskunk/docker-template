## Docker-template

Basic docker-compose config with custom PHP dockerfile.

Installed images : 
- mysql
- phpmyadmin:5.1.1-apache
- maildev/maildev
- php:7.2-apache (custom PHP Dockerfile)
  - certificate suuport : cacert.pem (from Tue Oct 11)
  - packages : git
  - php extensions : intl gd fileinfo mbstring mysqli pdo pdo_mysql xsl opcache zip xml calendar dom  bcmath bz2 / apcu xdebug
  - apache mods : proxy proxy_http proxy_fcgi setenvif rewrite authz_core expires ssl headers deflate
  - ufw : ports 80/tcp & 443/tcp
  - composer : v1.x
  - node (+npm) : v16.x
